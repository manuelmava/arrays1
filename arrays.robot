*** settings ***
DOCUMENTATION       Library is case sensitive


library     string
library     SeleniumLibrary

*** variable ***

${browser}           chrome
${homepage}          automationpractice.com/index.php
${scheme}            http
${testurl}           ${scheme}://${homepage}

*** keywords ***
Open homepage
    Open Browser    ${testurl}      ${browser}


***test cases***

TC001 Click in product container

    Open homepage
    set global variable         @{nombreDeContenedores}         //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a      //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a      //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a      //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a      //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a      //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    :FOR    ${nombreDeContenedor}       IN      @{nombreDeContenedores}
    \   click element     xpath=${nombre de contenedor}
    \   wait until element is visible          xpath=//*[@id="bigpic"]
    \   click element                          xpath=//*[@id="header_logo"]/a/img
    close browser